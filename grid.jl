include("alltypes.jl")

# function to calculate coefficients of transformation matrix and vector for specified cell
function calcualteCoefTransMatrixVec(points, cells, i)

    # find indices of points
    i1 = cells[i,1]
    i2 = cells[i,2]
    i3 = cells[i,3]
    # find coordinates of points
    a1x = points[i1,1]
    a1y = points[i1,2]
    a2x = points[i2,1]
    a2y = points[i2,2]
    a3x = points[i3,1]
    a3y = points[i3,2]
    # calculate coefficients of transformation matrix
    A11 = a2x - a1x
    A12 = a3x - a1x
    A21 = a2y - a1y
    A22 = a3y - a1y
    # calculate coefficients of transformation vector
    b1 = a1x
    b2 = a1y

    # return received coefficients
    return A11, A12, A21, A22, b1, b2

end

# function to update information about transformation function
function updateTrafoInformation(grid)

    # initialization
    Ncell = size(grid.cells, 1)
    cells = grid.cells
    points = grid.points

    # go through all cells
    for i in 1:Ncell
        # calculate coefficients of transformation matrix
        A11, A12, A21, A22, _, _ = calcualteCoefTransMatrixVec(points, cells, i)
        # calculate and save determinant of transformation matrix
        det = -A12 * A21 + A11 * A22
        grid.absDet[i] = abs(det)
        # calculate coefficients of transposed inversed matrix
        Ait11 = A22 / det
        Ait12 = -A21 / det
        Ait21 = -A12 / det
        Ait22 = A11 / det
        # save coefficients of transposed inversed matrix
        grid.invJac[i,1,1] = Ait11
        grid.invJac[i,1,2] = Ait12
        grid.invJac[i,2,1] = Ait21
        grid.invJac[i,2,2] = Ait22
    end

end

# function to calculate reference map for array of points in reference element
function calculateReferenceMap(grid, xHat)

    # initialization
    Ncell = size(grid.cells, 1)
    Np = size(xHat, 1)
    points = grid.points
    cells = grid.cells
    # create array to save coordinates of points in new triangles
    xNew = Array{Float64, 3}(undef, Ncell, Np, 2)

    # go through all cells
    for i in 1:Ncell
        # calculate coefficients of transformation matrix and vector
        A11, A12, A21, A22, b1, b2 = calcualteCoefTransMatrixVec(points, cells, i)
        # go through all points
        for j in 1:Np
            # find coordinates of current point
            xcur = xHat[j,1]
            ycur = xHat[j,2]
            # calculate coordinates in new triangle
            xnew = A11 * xcur + A12 * ycur + b1
            ynew = A21 * xcur + A22 * ycur + b2
            # save new coordinates
            xNew[i,j,1] = xnew
            xNew[i,j,2] = ynew
        end
    end

    # return array of coordinates in new triangles
    return xNew

end

# function to define boundary points
function defineBoundaryPoints(grid)

    # initialization
    points = grid.points
    Np = size(points, 1)

    # go through all points
    for i in 1:Np
        # find out coordinates of point
        xc = points[i,1]
        yc = points[i,2]
        # check whether the current point is on boundary
        if grid.isBoundaryPoint(xc, yc)
            append!(grid.boundaryPoints, [i])
        else
            append!(grid.freePoints, [i])
        end
    end

end
