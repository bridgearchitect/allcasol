# define default constants
noIndex = -1
noString = ""
smallNumber = 0.00001
# define constants of mathematical solver
order = 1
# define constants of physical system
a = 1.0
c = 0.0
f0 = 0.0
lambda = -1.0
minInitValue = -1.0
maxInitValue = 1.0
gamma = 1.0
epsilon = 0.1
# define simulation constants
N = 600
freqUpdate = 20
tau = 0.0005
# define string constants
filenameBaseInter = "sim"
filenameRes = "final.dat"

# define function to describe boundary of explored region
function isBoundaryPoint(x, y)
    return false
    # return abs(1.0 - sqrt(x^2 + y^2)) <= smallNumber
end

# define function to describe external source of physical system
function externalSourceFunction(x, y)
    return f0
end

# define function to describe boundary value function of physical system
function boundaryValueFunction(x, y)
    return lambda
end

# define function to specify initial value for non-stationary problem
function initValueFunction(x, y)
    return (-1.0) * tanh((sqrt(x^2 + y^2) - 0.45) / (sqrt(2.0) * epsilon))
end

# function to calculate L2 distance between points
function calculateDistanceL2TwoPoints(x1, y1, x2, y2)
    return sqrt((x2 - x1)^2 + (y2 - y1)^2)
end

# define grid structure
mutable struct Grid
    points::Array{Float64, 2}
    cells::Array{Int, 2}
    invJac::Array{Float64, 3}
    absDet::Array{Float64, 1}
    boundaryPoints::Array{Int, 1}
    freePoints::Array{Int, 1}
    isBoundaryPoint::Function
end
# default constuctor
Grid(points, cells, isBoundaryPoint) = Grid(points, cells, Array{Float64, 3}(undef, size(cells, 1), 2, 2),
                           Array{Float64, 1}(undef, size(cells, 1)), Array{Int, 1}(undef, 0),
                           Array{Int, 1}(undef, 0), isBoundaryPoint)

# define structure for integration rule
mutable struct IntegrationRule
    weights::Array{Float64, 1}
    points::Array{Float64, 2}
end
# default constuctor
IntegrationRule(Np) = IntegrationRule(Array{Float64, 1}(undef, Np),  Array{Float64, 2}(undef, Np, 2))

# define structure for global basis
mutable struct GlobalBasis
    grid::Grid
end
# default constuctor
GlobalBasis(grid) = GlobalBasis(grid)

# define structure to describe stationary problem
mutable struct StationaryProblem
    grid::Grid
    globalBasis::GlobalBasis
    integrationRule::IntegrationRule
    a::Float64
    c::Float64
    l::Float64
    t::Float64
    diffusion::Array{Float64, 2}
    mass::Array{Float64, 2}
    massNonLinear::Array{Float64, 2}
    systemMatrix::Array{Float64, 2}
    systemVector::Array{Float64, 1}
    solution::Array{Float64, 1}
    xkHat::Array{Float64, 2}
    xkTrans::Array{Float64, 3}
    wkHat::Array{Float64, 1}
    phi::Array{Float64, 2}
    lValues::Array{Float64, 2}
    tValues::Array{Float64, 2}
    gradPhi::Array{Float64, 3}
end
StationaryProblem(grid, globalBasis, integrationRule, a, c, l, t, Np) = StationaryProblem(grid, globalBasis,
                                                                                 integrationRule, a, c, l, t,
                                                                                 Array{Float64, 2}(undef, Np, Np),
                                                                                 Array{Float64, 2}(undef, Np, Np),
                                                                                 Array{Float64, 2}(undef, Np, Np),
                                                                                 Array{Float64, 2}(undef, Np, Np),
                                                                                 Array{Float64, 1}(undef, Np),
                                                                                 Array{Float64, 1}(undef, Np),
                                                                                 Array{Float64, 2}(undef, 0, 0),
                                                                                 Array{Float64, 3}(undef, 0, 0, 0),
                                                                                 Array{Float64, 1}(undef, 0),
                                                                                 Array{Float64, 2}(undef, 0, 0),
                                                                                 Array{Float64, 2}(undef, 0, 0),
                                                                                 Array{Float64, 2}(undef, 0, 0),
                                                                                 Array{Float64, 3}(undef, 0, 0, 0))
