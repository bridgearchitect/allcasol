(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 13.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      3572,        114]
NotebookOptionsPosition[      3072,         97]
NotebookOutlinePosition[      3493,        113]
CellTagsIndexPosition[      3450,        110]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"dataAll", "=", 
   RowBox[{
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{"#", "[", 
        RowBox[{"[", "1", "]"}], "]"}], ",", 
       RowBox[{"#", "[", 
        RowBox[{"[", "3", "]"}], "]"}]}], "}"}], "&"}], "/@", 
    RowBox[{"Delete", "[", 
     RowBox[{
      RowBox[{
      "Import", "[", 
       "\"\</home/bridgearchitect/allcasol/analysisDomain/final.dat\>\"", 
       "]"}], ",", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"{", "1", "}"}], ",", 
        RowBox[{"{", "2", "}"}]}], "}"}]}], "]"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"dataConc", "=", 
   RowBox[{
    RowBox[{
     RowBox[{"#", "[", 
      RowBox[{"[", "3", "]"}], "]"}], "&"}], "/@", 
    RowBox[{"Delete", "[", 
     RowBox[{
      RowBox[{
      "Import", "[", 
       "\"\</home/bridgearchitect/allcasol/analysisDomain/final.dat\>\"", 
       "]"}], ",", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"{", "1", "}"}], ",", 
        RowBox[{"{", "2", "}"}]}], "}"}]}], "]"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"maxConc", "=", 
   RowBox[{"Max", "[", "dataConc", "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"dataAll", "=", 
   RowBox[{
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{"#", "[", 
        RowBox[{"[", "1", "]"}], "]"}], ",", 
       RowBox[{
        RowBox[{"#", "[", 
         RowBox[{"[", "2", "]"}], "]"}], "/", "maxConc"}]}], "}"}], "&"}], "/@",
     "dataAll"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"FindFit", "[", 
  RowBox[{"dataAll", ",", 
   RowBox[{"Tanh", "[", 
    RowBox[{"x", "/", "l"}], "]"}], ",", 
   RowBox[{"{", "l", "}"}], ",", "x"}], "]"}]}], "Input",
 CellChangeTimes->{{3.862364052288827*^9, 3.862364338548833*^9}, {
  3.8623646171692677`*^9, 3.862364620886586*^9}, {3.862364669957164*^9, 
  3.862364693865217*^9}, {3.862364856896143*^9, 3.862364875469523*^9}, {
  3.862372365361393*^9, 3.862372367443006*^9}},
 CellLabel->"In[68]:=",ExpressionUUID->"23be2fc7-61e0-40d0-ae00-d8d568e58c5c"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"l", "\[Rule]", "0.29120323879925314`"}], "}"}]], "Output",
 CellChangeTimes->{{3.86236434106254*^9, 3.862364344827932*^9}, {
   3.862364621696496*^9, 3.862364625573152*^9}, {3.862364672885539*^9, 
   3.862364695403268*^9}, 3.862364876250041*^9, 3.862372328580866*^9, 
   3.8623723683423033`*^9},
 CellLabel->"Out[72]=",ExpressionUUID->"d02fb659-c4a4-4a6b-9e23-a76bf9da7f36"]
}, Open  ]]
},
WindowSize->{1355.2941176470588`, 741.8823529411765},
WindowMargins->{{0, Automatic}, {0, Automatic}},
FrontEndVersion->"13.0 for Linux x86 (64-bit) (December 2, 2021)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"22658b5a-e277-4cdd-bdae-8339c6ef91f0"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 2059, 63, 111, "Input",ExpressionUUID->"23be2fc7-61e0-40d0-ae00-d8d568e58c5c"],
Cell[2642, 87, 414, 7, 55, "Output",ExpressionUUID->"d02fb659-c4a4-4a6b-9e23-a76bf9da7f36"]
}, Open  ]]
}
]
*)

