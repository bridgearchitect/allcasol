include("alltypes.jl")
include("grid.jl")
include("integration.jl")
include("basis.jl")
include("write.jl")

# function to create object to describe stationary problem
function createStationaryProblem(grid, globalBasis, integrationRule, a, c, l, t)

    # find out number of points
    Np = size(grid.points, 1)
    # create object
    stationaryProblem = StationaryProblem(grid, globalBasis, integrationRule, a, c, l, t, Np)

    # initialize matrices for description of physical system
    for i in 1:Np
        for j in 1:Np
            stationaryProblem.diffusion[i,j] = 0.0
            stationaryProblem.mass[i,j] = 0.0
            stationaryProblem.massNonLinear[i,j] = 0.0
            stationaryProblem.systemMatrix[i,j] = 0.0
        end
    end

    # initialize vectors for description of physical system
    for i in 1:Np
        stationaryProblem.systemVector[i] = 0.0
        stationaryProblem.solution[i] = 0.0
    end

    # find out boundary nodes
    defineBoundaryPoints(stationaryProblem.grid)
    # find information about transformation function
    updateTrafoInformation(stationaryProblem.grid)

    # save data about integration rule
    stationaryProblem.xkHat = integrationRule.points
    stationaryProblem.xkTrans = calculateReferenceMap(stationaryProblem.grid, stationaryProblem.xkHat)
    stationaryProblem.wkHat = integrationRule.weights

    # save data about global basis functions
    stationaryProblem.phi = calculatePhi(stationaryProblem.xkHat)
    stationaryProblem.gradPhi = calculateGradPhi(stationaryProblem.xkHat)

    # save data about "l" and "t" functions
    stationaryProblem.lValues, stationaryProblem.tValues = calculateLandTValues(grid,
    stationaryProblem.solution, stationaryProblem.xkHat)

    # return created object to describe stationary problem
    return stationaryProblem

end

# function to add external source into stationary physical problem
function addSource(stationaryProblem, externalSourceFunction)

    # initialization
    freePoints = stationaryProblem.grid.freePoints
    basis = stationaryProblem.globalBasis
    grid = stationaryProblem.grid
    wkHat = stationaryProblem.wkHat
    phi = stationaryProblem.phi
    xkTrans = stationaryProblem.xkTrans
    # find out number of free points
    Nfree = size(stationaryProblem.grid.freePoints, 1)

    # go throgh all free points
    for i in 1:Nfree
        # calculate DOF map
        supp, localInd = calculateDOFMap(basis, freePoints[i])
        # go through all cells of support
        for j in 1:size(supp, 1)
            # find value of determinant for transformation function
            detCell = grid.absDet[supp[j]]
            for intIndex in 1:size(wkHat,1)
                # update value of system vector
                stationaryProblem.systemVector[freePoints[i]] += detCell *
                phi[intIndex, localInd[j]] * wkHat[intIndex] *
                externalSourceFunction(xkTrans[supp[j], intIndex, 1], xkTrans[supp[j], intIndex, 2])
            end
        end
    end

end

# function to add external source into non-stationary physical problem
function addDiscreteNonLinearSource(stationaryProblem)

    # initialization
    freePoints = stationaryProblem.grid.freePoints
    basis = stationaryProblem.globalBasis
    grid = stationaryProblem.grid
    wkHat = stationaryProblem.wkHat
    phi = stationaryProblem.phi
    xkTrans = stationaryProblem.xkTrans
    tValues = stationaryProblem.tValues
    t = stationaryProblem.t
    # find out number of free points
    Nfree = size(stationaryProblem.grid.freePoints, 1)

    # go throgh all free points
    for i in 1:Nfree
        # calculate DOF map
        supp, localInd = calculateDOFMap(basis, freePoints[i])
        # go through all cells of support
        for j in 1:size(supp, 1)
            # find value of determinant for transformation function
            detCell = grid.absDet[supp[j]]
            for intIndex in 1:size(wkHat,1)
                # update value of system vector
                stationaryProblem.systemVector[freePoints[i]] += detCell * t *
                phi[intIndex, localInd[j]] * wkHat[intIndex] * tValues[supp[j], intIndex]
            end
        end
    end

end

# function to add new discrete source for linear system
function addDiscreteLinearSource(stationaryProblem, vector)

    # initialization
    mass = stationaryProblem.mass
    # update value
    stationaryProblem.systemVector += mass * vector

end

# function to assemble boundary conditions
function assembleBoundaryCondition(stationaryProblem, boundaryValueFunction)

    # initialization
    boundaryPoints = stationaryProblem.grid.boundaryPoints
    points = stationaryProblem.grid.points

    # go through all boundary points
    for i in 1:size(boundaryPoints, 1)
        # receive index of boundary point
        indBound = boundaryPoints[i]
        # find out coordinates of the given boundary point
        xcur = points[indBound, 1]
        ycur = points[indBound, 2]
        # change values for system vector and matrix
        stationaryProblem.systemVector[indBound] = boundaryValueFunction(xcur, ycur)
        stationaryProblem.systemMatrix[indBound,indBound] = 1.0
    end

end

# function to assemble system matrix for stationary problem
function assambleSystemMatrix(stationaryProblem)

    # initialization
    a = stationaryProblem.a
    c = stationaryProblem.c
    l = stationaryProblem.l
    diffusion = stationaryProblem.diffusion
    mass = stationaryProblem.mass
    massNonLinear = stationaryProblem.massNonLinear
    # assemble matrix
    stationaryProblem.systemMatrix += a * diffusion + c * mass + l * massNonLinear

end

# function to initialize diffusion matrix for stationary problem
function initializeDiffusion(stationaryProblem)

    # initialization
    wkHat = stationaryProblem.wkHat
    gradPhi = stationaryProblem.gradPhi
    basis = stationaryProblem.globalBasis
    freePoints = stationaryProblem.grid.freePoints
    grid = stationaryProblem.grid
    # find out number of all and free points
    Nfree = size(stationaryProblem.grid.freePoints, 1)
    Np = size(stationaryProblem.grid.points, 1)

    # go through all free points
    for i in 1:Nfree
        # go through all points
        for j in 1:Np
            # reset value for initialization
            stationaryProblem.diffusion[freePoints[i], j] = 0.0
        end
    end

    # go through all free points
    for i in 1:Nfree
        # go through all points
        for j in 1:Np
            # find all cells of support and calculate DOF map for intersection of two points
            cellsIndexes, dofMapI, dofMapJ = calculateSharedGlobalPhi(basis, freePoints[i], j)
            # go through all cells of intersection of supports
            for k in 1:size(cellsIndexes, 1)
                # save indexes
                dofIndexI = dofMapI[k]
                dofIndexJ = dofMapJ[k]
                cellIndex = cellsIndexes[k]
                # save determinant of Jacobian matrix
                adetCell = grid.absDet[cellIndex]
                # save matrix of inverse transformation for the given cell
                invMatrix = grid.invJac[cellIndex, :, :]
                for intIndex in 1:size(wkHat,1)
                    # save gradients of local basis functions
                    gradVectorI = gradPhi[intIndex, dofIndexI, :]
                    gradVectorJ = gradPhi[intIndex, dofIndexJ, :]
                    # multiply matrix of inverse transformation on gradient of local basis functions
                    finVectorI = invMatrix * gradVectorI
                    finVectorJ = invMatrix * gradVectorJ
                    # numerically integrate component of mass matrix
                    stationaryProblem.diffusion[freePoints[i], j] += adetCell * wkHat[intIndex] *
                    sum(finVectorI .* finVectorJ)
                end
            end
        end
    end

end

# function to initialize mass matrix for stationary problem
function initializeMass(stationaryProblem)

    # initialization
    wkHat = stationaryProblem.wkHat
    phi = stationaryProblem.phi
    grid = stationaryProblem.grid
    basis = stationaryProblem.globalBasis
    freePoints = stationaryProblem.grid.freePoints
    # find out number of free and all points
    nFree = size(grid.freePoints, 1)
    n = size(grid.points, 1)

    # go through all free points
    for i in 1:nFree
        # go through all points
        for j in 1:n
            # reset value for initialization
            stationaryProblem.mass[freePoints[i], j] = 0.0
        end
    end

    # go through all free points
    for i in 1:nFree
        # go through all points
        for j in 1:n
            # find all cells of support and calculate DOF map for intersection of two points
            cellsIndexes, dofMapI, dofMapJ = calculateSharedGlobalPhi(basis, freePoints[i], j)
            # go through all cells of intersection of supports
            for k in 1:size(cellsIndexes, 1)
                # save indexes
                dofIndexI = dofMapI[k]
                dofIndexJ = dofMapJ[k]
                cellIndex = cellsIndexes[k]
                # save determinant of Jacobian matrix
                adetCell = grid.absDet[cellIndex]
                # go through all points of integration
                for intIndex in 1:size(wkHat, 1)
                    # numerically integrate component of mass matrix
                    stationaryProblem.mass[freePoints[i], j] += adetCell * wkHat[intIndex] *
                    phi[intIndex, dofIndexI] * phi[intIndex, dofIndexJ]
                end
            end
        end
    end

end

# function to initialize nonlinear mass matrix for stationary problem
function initializeNonLinearMass(stationaryProblem)

    # initialization
    wkHat = stationaryProblem.wkHat
    phi = stationaryProblem.phi
    grid = stationaryProblem.grid
    basis = stationaryProblem.globalBasis
    freePoints = stationaryProblem.grid.freePoints
    lValues = stationaryProblem.lValues
    l = stationaryProblem.l
    # find out number of free and all points
    nFree = size(grid.freePoints, 1)
    n = size(grid.points, 1)

    # go through all free points
    for i in 1:nFree
        # go through all points
        for j in 1:n
            # reset value for initialization
            stationaryProblem.massNonLinear[freePoints[i], j] = 0.0
        end
    end

    # go through all free points
    for i in 1:nFree
        # go through all points
        for j in 1:n
            # find all cells of support and calculate DOF map for intersection of two points
            cellsIndexes, dofMapI, dofMapJ = calculateSharedGlobalPhi(basis, freePoints[i], j)
            # go through all cells of intersection of supports
            for k in 1:size(cellsIndexes, 1)
                # save indexes
                dofIndexI = dofMapI[k]
                dofIndexJ = dofMapJ[k]
                cellIndex = cellsIndexes[k]
                # save determinant of Jacobian matrix
                adetCell = grid.absDet[cellIndex]
                # go through all points of integration
                for intIndex in 1:size(wkHat, 1)
                    # numerically integrate component of mass matrix
                    stationaryProblem.massNonLinear[freePoints[i], j] += adetCell * wkHat[intIndex] *
                    phi[intIndex, dofIndexI] * phi[intIndex, dofIndexJ] * lValues[cellIndex, intIndex]
                end
            end
        end
    end

end

# function to initialize value of solution
function initializeSolution(stationaryProblem, initValueFunction)

    # initialization
    grid = stationaryProblem.grid
    numFreePoints = size(grid.freePoints, 1)

    # go through all free points
    for i in 1:numFreePoints
        # find out index of free point
        index = grid.freePoints[i]
        # find coordinates of free point
        xcur = grid.points[index, 1]
        ycur = grid.points[index, 2]
        # updata value for solution
        stationaryProblem.solution[index] = initValueFunction(xcur, ycur)
    end

end

# function to reset system vector for stationary problem
function resetSystemVector(stationaryProblem)

    # reset system vector
    nPoints = size(stationaryProblem.systemVector, 1)
    for i in 1:nPoints
        stationaryProblem.systemVector[i] = 0.0
    end

end

# function to reset system matrix for stationary problem
function resetSystemMatrix(stationaryProblem)

    # reset system matrix
    nPoints = size(stationaryProblem.systemVector, 1)
    for i in 1:nPoints
        for j in 1:nPoints
            stationaryProblem.systemMatrix[i, j] = 0.0
        end
    end

end

# function to reset nonlinear mass matrix for stationary problem
function resetNonLinearMass(stationaryProblem)

    # reset nonlinear mass matrix
    nPoints = size(stationaryProblem.systemVector, 1)
    for i in 1:nPoints
        for j in 1:nPoints
            stationaryProblem.massNonLinear[i, j] = 0.0
        end
    end

end

# function to solve specified stationary problem
function solveStationaryProblem(stationaryProblem)
    # solve system of linear equations
    stationaryProblem.solution = stationaryProblem.systemMatrix \ stationaryProblem.systemVector
end

# function to solve non-stationary problem using object for stationary problem
function solveNonStationaryProblem(stationaryProblem, N, tau, externalSourceFunction,
                                   boundaryValueFunction, freqUpdate, filenameBaseInter)

    # initialization
    uArray = stationaryProblem.solution
    grid = stationaryProblem.grid
    xHat = stationaryProblem.xkHat

    # write intermediate results
    writeIntermediateResults(stationaryProblem, filenameBaseInter, Float64(0) * tau, 0)

    # launch loop for non-stationary problem
    for i in 1:N
        # add source to problem
        addSource(stationaryProblem, externalSourceFunction)
        # update arrays for "l" and "t" vectors
        stationaryProblem.lValues, stationaryProblem.tValues = calculateLandTValues(grid, uArray, xHat)
        # update system vector for instationary problem (linear part)
        addDiscreteLinearSource(stationaryProblem, uArray / tau)
        # update system vector for instationary problem (non-linear part)
        addDiscreteNonLinearSource(stationaryProblem)
        # assemble boundary condition
        assembleBoundaryCondition(stationaryProblem, boundaryValueFunction)
        # initialize matrix for non-linear mass
        initializeNonLinearMass(stationaryProblem)
        # assemble new system matrix for non-stationary problem
        assambleSystemMatrix(stationaryProblem)
        # solve stationary problem
        solveStationaryProblem(stationaryProblem)
        # receive new array of values
        uArray = stationaryProblem.solution
        # reset system vector and matrix
        resetSystemVector(stationaryProblem)
        resetSystemMatrix(stationaryProblem)
        # reset nonlinear mass matrix
        resetNonLinearMass(stationaryProblem)
        # write intermediate results if it is essential
        if (i - 1) % freqUpdate == 0
            writeIntermediateResults(stationaryProblem, filenameBaseInter, Float64(i) * tau, i)
        end
    end

end
