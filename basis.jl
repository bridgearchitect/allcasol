include("alltypes.jl")
include("grid.jl")

# function to get value function for interpolation
function getValueFunctionInterpolation(u1, u2, u3, x1, y1, x2, y2, x3, y3, xp, yp)

    # calculate distances from specified points to triangle vertices
    dist1 = sqrt((xp - x1)^2 + (yp - y1)^2)
    dist2 = sqrt((xp - x2)^2 + (yp - y2)^2)
    dist3 = sqrt((xp - x3)^2 + (yp - y3)^2)
    # find interpolation value of function using calculated distances
    uInter = (u1 * dist1 + u2 * dist2 + u3 * dist3) / (dist1 + dist2 + dist3)
    # return recieved value
    return uInter

end

# function to calculate local basis function for array of points
function calculatePhi(xHat)

    # initialization
    Np = size(xHat, 1)
    phi = Array{Float64, 2}(undef, Np, 3)

    # go through all points
    for i in 1:Np
        phi[i,1] = 1.0 - xHat[i,1] - xHat[i,2]
        phi[i,2] = xHat[i,1]
        phi[i,3] = xHat[i,2]
    end

    # return array of values
    return phi

end

# function to calculate gradient of local basis function for array of points
function calculateGradPhi(xHat)

    # initialization
    Np = size(xHat, 1)
    gradPhi = Array{Float64, 3}(undef, Np, 3, 2)

    # go through all points
    for i in 1:Np
        gradPhi[i,1,1] = -1.0
        gradPhi[i,1,2] = -1.0
        gradPhi[i,2,1] = 1.0
        gradPhi[i,2,2] = 0.0
        gradPhi[i,3,1] = 0.0
        gradPhi[i,3,2] = 1.0
    end

    # return array of gradients
    return gradPhi

end

# function to calculate values of "l" and "t" functions
function calculateLandTValues(grid, uVector, xHat)

    # initialization
    points = grid.points
    cells = grid.cells
    Ncell = size(cells, 1)
    Np = size(xHat, 1)
    # create array to save "l" and "t" values
    lValues = Array{Float64, 2}(undef, Ncell, Np)
    tValues = Array{Float64, 2}(undef, Ncell, Np)

    # go through all cells
    for i in 1:Ncell
        # recieve indexes of triangle vertices
        index1 = cells[i,1]
        index2 = cells[i,2]
        index3 = cells[i,3]
        # receive coordinates of triangle vertices
        x1 = points[index1, 1]
        y1 = points[index1, 1]
        x2 = points[index2, 1]
        y2 = points[index2, 1]
        x3 = points[index3, 1]
        y3 = points[index3, 1]
        # receive values of functions "u" on the vertix of triangle
        u1 = uVector[index1]
        u2 = uVector[index2]
        u3 = uVector[index3]
        # calculate coefficients of transformation matrix
        A11, A12, A21, A22, b1, b2 = calcualteCoefTransMatrixVec(points, cells, i)
        # go through integration points
        for j in 1:Np
            # receive coordinates of integration point
            xIntHat = xHat[j, 1]
            yIntHat = xHat[j, 2]
            # calculate coordinates of point in original triangle
            xInt = A11 * xIntHat + A12 * yIntHat + b1
            yInt = A21 * xIntHat + A22 * yIntHat + b2
            # receive interpolated value of "u" function
            uInt = getValueFunctionInterpolation(u1, u2, u3, x1, y1, x2, y2, x3, y3, xInt, yInt)
            # to debug code
            # println(uInt, " ", u1, " ", u2, " ", u3)
            # sleep(0.1)
            # calculate values of "l" and "t" function using interpolated value
            lValues[i, j] = 3.0 * uInt * uInt - 1.0
            tValues[i, j] = -2.0 * uInt * uInt * uInt
        end
    end

    # return created arrays of values
    return lValues, tValues

end

# function to calculate DOF map
function calculateDOFMap(basis, globalInd)

    # initialization
    cells = basis.grid.cells
    Nc = size(cells, 1)
    Np = size(cells, 2)
    supp = Array{Int, 1}(undef, 0)
    localInd = Array{Int, 1}(undef, 0)

    # go through all cells
    for i in 1:Nc
        # go through all points
        for j in 1:Np
            # check whether it is essential point
            if cells[i, j] == globalInd
                # add new point in arrays of support and local indeces
                append!(supp, [i])
                append!(localInd, [j])
            end
        end
    end

    # return arrays of support and local indeces
    return supp, localInd

end

# function to calculate global basis functions for array of points
function calculateGlobalPhi(basis, xHat, globalInd)

    # receive arrays of support and local indeces
    _, localInd = calculateDOFMap(basis, globalInd)
    # calculate array of values
    valHat = calculatePhi(xHat)
    # create array to save values of global basis function
    Np = size(valHat, 1)
    Nl = size(localInd, 1)
    valGlobalHat = Array{Float64, 2}(undef, Nl, Np)

    # fill array of global basis function
    for i in 1:Nl
        for j in 1:Np
            valGlobalHat[i,j] = valHat[j, localInd[i]]
        end
    end

    # return array of global basis function
    return valGlobalHat

end

# function to calculate global basis functions for intersection of two elements
function calculateSharedGlobalPhi(basis, globalIndI, globalIndJ)

    # calculate arrays of support and local indices for two elements
    suppI, localIndicesI = calculateDOFMap(basis, globalIndI)
    suppJ, localIndicesJ = calculateDOFMap(basis, globalIndJ)

    # create additional arrays
    suppIJ = Array{Int, 1}(undef, 0)
    findArrayI = Array{Int, 1}(undef, 0)
    findArrayJ = Array{Int, 1}(undef, 0)
    localIndicesRedI = Array{Int, 1}(undef, 0)
    localIndicesRedJ = Array{Int, 1}(undef, 0)

    # find intersection support
    for i in 1:size(suppI, 1)
        for j in 1:size(suppJ, 1)
            # check whether two cells are equal
            if suppI[i] == suppJ[j]
                # add new cell in intersection array
                append!(suppIJ, [suppI[i]])
                append!(findArrayI, [i])
                append!(findArrayJ, [j])
                break
            end
        end
    end

    # find reduced array
    for i in 1:size(suppIJ, 1)
        append!(localIndicesRedI, [localIndicesI[findArrayI[i]]])
        append!(localIndicesRedJ, [localIndicesJ[findArrayJ[i]]])
    end

    # return arrays of support and local indices for intersection
    return suppIJ, localIndicesRedI, localIndicesRedJ

end
