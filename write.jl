include("alltypes.jl")

# function write intermediate results in seperate file
function writeIntermediateResults(stationaryProblem, filenameBaseInter, curTime, idIter)

    # initialization
    points = stationaryProblem.grid.points
    solution = stationaryProblem.solution

    # open file
    file = open(filenameBaseInter * string(idIter) * ".dat", "w")
    # write header row
    write(file, "time = " * string(curTime) * "\n")
    write(file, "x y u\n")
    # initialization
    Np = size(points, 1)

    # go through all points
    for i in 1:Np
        # receive coordinates of point
        xcur = points[i,1]
        ycur = points[i,2]
        # receive found value of function in the given point
        ucur = solution[i]
        # write new row
        write(file, string(xcur) * " " * string(ycur) * " " * string(ucur) * "\n")
    end

    # close file
    close(file)

end

# function to write final solution in file
function writeFinalSolution(points, solution, filenameRes)

    # open file
    file = open(filenameRes, "w")
    # write header row
    write(file, "x y u\n")
    # initialization
    Np = size(points, 1)

    # go through all points
    for i in 1:Np
        # receive coordinates of point
        xcur = points[i,1]
        ycur = points[i,2]
        # receive found value of function in the given point
        ucur = solution[i]
        # write new row
        write(file, string(xcur) * " " * string(ycur) * " " * string(ucur) * "\n")
    end

    # close file
    close(file)

end
