include("alltypes.jl")

# function to read name of input mesh file
function readFilename()

    # read name of files
    println("Name of mesh file:")
    filenameMesh = readline()
    # return name of files
    return filenameMesh

end

# function to read mesh file
function readMeshFile(filenameMesh)

    # read mesh file
    cellsFull, pointsFull = read_mesh(filenameMesh)
    # find out number of points
    numPoints = size(pointsFull, 1)
    # find out index and number of triangle cells
    indexTri = size(cellsFull.type, 1)
    numCells = size(cellsFull.index[indexTri], 1)

    # create arrays to save points and cells
    cells = Array{Int, 2}(undef, numCells, 3)
    points = Array{Float64, 2}(undef, numPoints - 1, 2)

    # save points in created array
    for i in 2:numPoints
        points[i - 1, 1] = pointsFull[i,1]
        points[i - 1, 2] = pointsFull[i,2]
        i = i + 1
    end

    # save cells in created array
    arrayIndexes = cellsFull.index[indexTri]
    for i in 1:numCells
        cells[i,1] = arrayIndexes[i,1] - 1
        cells[i,2] = arrayIndexes[i,2] - 1
        cells[i,3] = arrayIndexes[i,3] - 1
    end

    # return received points and cells
    return points, cells

end

# function to sort cell indexes in array
function sortCells(points, cells)

    # initialization
    Nc = size(cells, 1)

    # go through all cells
    for i in 1:Nc
        # find index of points
        ind1 = cells[i, 1]
        ind2 = cells[i, 2]
        ind3 = cells[i, 3]
        # find coordinates of points
        x1 = points[ind1, 1]
        y1 = points[ind1, 2]
        x2 = points[ind2, 1]
        y2 = points[ind2, 2]
        x3 = points[ind3, 1]
        y3 = points[ind3, 2]
        # calculate center of cell
        xc = (x1 + x2 + x3) / 3.0
        yc = (y1 + y2 + y3) / 3.0
        # calculate distance between points
        dist1 = calculateDistanceL2TwoPoints(x1, y1, x2, y2)
        dist2 = calculateDistanceL2TwoPoints(x2, y2, x3, y3)
        dist3 = calculateDistanceL2TwoPoints(x3, y3, x1, y1)
        # find side with maximal length
        maxDist = max(dist1, dist2, dist3)
        # swap cells if it is essential
        maxIndex = noIndex
        if maxDist == dist1
            maxIndex = 1
        elseif maxDist == dist2
            maxIndex = 2
        elseif maxDist == dist3
            maxIndex = 3
        end
        # calculate angles for all three points from center of triangle
        phi1 = atan(y1 - yc, x1 - xc)
        phi2 = atan(y2 - yc, x2 - xc)
        phi3 = atan(y3 - yc, x3 - xc)
        # sort points using calculated angles
        newIndeces = sortperm([phi1, phi2, phi3])
        # choose correctly the first node
        swapIndeces = [0, 0, 0]
        while true
            # finish shifting
            if newIndeces[1] == maxIndex
                break
            end
            # find out indices of cell nodes
            swapIndeces[1] = newIndeces[1]
            swapIndeces[2] = newIndeces[2]
            swapIndeces[3] = newIndeces[3]
            # shift nodes
            newIndeces[1] = swapIndeces[3]
            newIndeces[2] = swapIndeces[1]
            newIndeces[3] = swapIndeces[2]
        end
        # save new indeces
        swapIndeces[1] = cells[i, 1]
        swapIndeces[2] = cells[i, 2]
        swapIndeces[3] = cells[i, 3]
        cells[i, 1] = swapIndeces[newIndeces[1]]
        cells[i, 2] = swapIndeces[newIndeces[2]]
        cells[i, 3] = swapIndeces[newIndeces[3]]
    end

end
