include("alltypes.jl")

# function to create integration rule
function createIntegrationRule(order)

    # initialization
    integrationRule = nothing

    # consider different situation
    if order == 1
        # create object to specify integration rule
        integrationRule = IntegrationRule(1)
        # fill weights
        integrationRule.weights[1] = 0.5
        # fill coordinates of points
        integrationRule.points[1,1] = 1.0 / 3.0
        integrationRule.points[1,2] = 1.0 / 3.0
    elseif order == 2
        # create object to specify integration rule
        integrationRule = IntegrationRule(3)
        # fill weights
        integrationRule.weights[1] = 1.0 / 6.0
        integrationRule.weights[2] = 1.0 / 6.0
        integrationRule.weights[3] = 1.0 / 6.0
        # fill coordinates of points
        integrationRule.points[1,1] = 1.0 / 6.0
        integrationRule.points[1,2] = 1.0 / 6.0
        integrationRule.points[2,1] = 2.0 / 3.0
        integrationRule.points[2,2] = 1.0 / 6.0
        integrationRule.points[3,1] = 1.0 / 6.0
        integrationRule.points[3,2] = 2.0 / 3.0
    end

    # return integration rule
    return integrationRule

end
