using FiniteMesh

# import local libraries
include("read.jl")
include("alltypes.jl")
include("grid.jl")
include("integration.jl")
include("basis.jl")
include("problem.jl")
include("write.jl")

# read name of files
filenameMesh = readFilename()
# read mesh file
points, cells = readMeshFile(filenameMesh)
# sort cell indexes inside special array
sortCells(points, cells)

# create grid structure
grid = Grid(points, cells, isBoundaryPoint)
# create object for integration rule
integrationRule = createIntegrationRule(order)
# create object to descibe global basis
globalBasis = GlobalBasis(grid)

# create object to describe stationary problem
stationaryProblem = createStationaryProblem(grid, globalBasis, integrationRule, gamma * epsilon, 1.0 / tau,
                                            gamma / epsilon, -gamma / epsilon)
# initialize value function
initializeSolution(stationaryProblem, initValueFunction)
# calculate diffusion matrix for stationary problem
initializeDiffusion(stationaryProblem)
# calculate mass matrix for stationary problem
initializeMass(stationaryProblem)
# solve non-stationary problem using object for stationary problem
solveNonStationaryProblem(stationaryProblem, N, tau, externalSourceFunction, boundaryValueFunction,
freqUpdate, filenameBaseInter)

# receive solution of physical system
solution = stationaryProblem.solution
points = grid.points
# write solution in text file
writeFinalSolution(points, solution, filenameRes)
